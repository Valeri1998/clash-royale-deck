//
//  AppDelegate.swift
//  ClashRoyaleDeck
//
//  Created by Валерий on 02.08.2020.
//  Copyright © 2020 Valeron.ink. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        setupInitialController()
        
        return true
    }
}

//MARK: - Private -
private extension AppDelegate {
    func setupInitialController() {
        let initialViewController = DeckViewController()

        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = initialViewController
        window?.makeKeyAndVisible()
    }
}
