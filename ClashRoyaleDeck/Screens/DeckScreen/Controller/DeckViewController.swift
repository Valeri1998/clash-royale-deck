import UIKit

class DeckViewController: UIViewController {

    override func loadView() {
        let deckView = DeckView.loadViewFromNib()
        view = deckView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}
