import UIKit

class DeckView: UIView {
    @IBOutlet var mainView: UIView!
    
    override func awakeFromNib() {
        setupMainView()
    }
}

//MARK: - Private -
private extension DeckView {
    func setupMainView() {
        mainView.frame = self.bounds
        addSubview(mainView)
    }
}
