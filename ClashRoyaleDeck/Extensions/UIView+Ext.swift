import Foundation
import UIKit

extension UIView {
    static func loadViewFromNib<T: UIView>() -> T? {
        let viewName = String(describing: self)
        let currentView = Bundle.main.loadNibNamed(viewName, owner: nil, options: nil)?.first as? T
        
        return currentView
    }
}
